# Free (as in freedom) food

This contains a collection of my favorite recipes typeset in LaTeX and licensed
under the GPL v.3. Most of these recipes are my own work.
When appropriate, I have cited the original recipe that I have modified.

## Don't Like LaTeX?

PDF copies of the compiled TeX files are located [here](https://bitbucket.org/bek7/food/downloads/).

## Contributing

If you have a good idea for a new recipe or an adjustment to one in the repo,
make a pull request to the `request` branch or submit a proposal to the issue
tracker.
